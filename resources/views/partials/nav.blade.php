<!-- Navbar -->
<div class="navbar-area sticky-top">
    <!-- Menu For Mobile Device -->
    <div class="mobile-nav">
        <a href="index.html" class="logo">
            <img src="{{ asset('/public/homepage/assets/img/logo-two.png') }}" alt="Logo">
        </a>
    </div>

    <!-- Menu For Desktop Device -->
    <div class="main-nav">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light">
                <a class="navbar-brand" href="index.html">
                    <img src="{{ asset('/public/homepage/assets/img/logo-two.png') }}" alt="Logo">
                </a>
                <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="{{route('home')}}" class="nav-link dropdown-toggle active">Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link dropdown-toggle">About <i class="icofont-simple-down"></i></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="{{route('about')}}" class="nav-link">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('members')}}" class="nav-link">Members</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('faq')}}" class="nav-link">FAQ</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('termscondition')}}" class="nav-link">Terms & Conditions</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('sponsor')}}" class="nav-link">Sponsor A Child</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('project')}}" class="nav-link">Project</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('newsstory')}}" class="nav-link">News & Stories</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('donate')}}" class="nav-link">Donate</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link dropdown-toggle">Get Involved <i class="icofont-simple-down"></i></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="{{route('sponsor')}}" class="nav-link">Sponsor A Child</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('donate')}}" class="nav-link">Donate</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('fundraiser')}}" class="nav-link">Fund Raiser</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('volunteer')}}" class="nav-link">Be A Volunteer</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('contact')}}" class="nav-link">Contact</a>
                        </li>
                    </ul>
                    <div class="side-nav">
                        <a class="donate-btn" href="{{route('donate')}}">
                            Donate
                            <i class="icofont-heart-alt"></i>
                        </a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
<!-- End Navbar -->
