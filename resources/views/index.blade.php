@extends('layouts.master')
@section('content')

    <!-- Preloader -->
    <div class="loader">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="pre-box-one">
                    <div class="pre-box-two"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Preloader -->

    @include('partials.header')
    @include('partials.nav')


    <!-- Banner -->
    <div class="banner-area-two three">
        <div class="banner-slider owl-theme owl-carousel">

            <div class="banner-slider-item banner-img-four">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="container">
                            <div class="banner-content">
                                <h1>We help all of the people in need around the world</h1>
                                <p>It is a long established fact that a reader will be page distracted by the readable content of a pain</p>
                                <div class="banner-btn-area">
                                    <a class="common-btn banner-btn" href="#">Get Start A Fundraising</a>
                                    <a class="common-btn" href="{{route('donate')}}">Donate Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="banner-slider-item banner-img-five">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="container">
                            <div class="banner-content">
                                <h1>Become a volunteer in our community to help people</h1>
                                <p>It is a long established fact that a reader will be page distracted by the readable content of a pain</p>
                                <div class="banner-btn-area">
                                    <a class="common-btn banner-btn" href="#">Get Start A Fundraising</a>
                                    <a class="common-btn" href="{{route('donate')}}">Donate Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--<div class="banner-slider-item banner-img-six">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="container">
                            <div class="banner-content">
                                <h1>We  are donating all over the world by our volunteer</h1>
                                <p>It is a long established fact that a reader will be page distracted by the readable content of a pain</p>
                                <div class="banner-btn-area">
                                    <a class="common-btn banner-btn" href="#">Get Start A Fundraising</a>
                                    <a class="common-btn" href="#">Donate Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}

        </div>
    </div>
    <!-- End Banner -->

    <!-- About -->
    <div class="about-area pt-100 pb-70">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-lg-6">
                    <div class="about-img">
                        <img src="{{ asset('/public/homepage/assets/img/about/about-main1.jpg')}}" alt="About">
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="about-content">
                        <div class="section-title">
                            <span class="sub-title">About us</span>
                            <h2>We're for social causes</h2>
                        </div>
                        <span class="about-span">We exist for non-profits, social enterprises, community groups, activists,lorem politicians and individual citizens that are making.</span>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore minima atque obcaecati deleniti tempora, cumque molestiae consectetur provident temporibus natus iste accusamus totam voluptas quas suscipit blanditiis fuga quibusdam porro.</p>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                        <div class="about-btn-area">
                            <a class="common-btn about-btn" href="#">Get Start A Fundraising</a>
                            <a class="common-btn" href="#">Read More</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End About -->

    <!-- Benefit -->
    <div class="benefit-area three pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">Core features</span>
                <h2>Our goals and missions</h2>
                <p>We exist for non-profits, social enterprises, community groups, activists,lorem politicians and individual citizens that are making.</p>
            </div>
            <div class="row">

                <div class="col-sm-6 col-lg-3">
                    <div class="benefit-item">
                        <i class="flaticon-house"></i>
                        <h3>Build home</h3>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Similique illum excepturi ab quam magnam earum</p>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="benefit-item two">
                        <i class="flaticon-hospital"></i>
                        <h3>Medical facilities</h3>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Similique illum excepturi ab quam magnam earum</p>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="benefit-item three">
                        <i class="flaticon-fast-food"></i>
                        <h3>Food & water</h3>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Similique illum excepturi ab quam magnam earum</p>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="benefit-item four">
                        <i class="flaticon-graduation-cap"></i>
                        <h3>Education facilities</h3>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Similique illum excepturi ab quam magnam earum</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End Benefit -->

    <!-- Donation -->
    <section class="donations-area three pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">Causes to care</span>
                <h2>Be the reason of someone smiles</h2>
                <p>We exist for non-profits, social enterprises, community groups, activists,lorem politicians and individual citizens that are making.</p>
            </div>
            <div class="row">

                <div class="col-sm-6 col-lg-4">
                    <div class="donation-item">
                        <div class="top">
                            <a class="tags" href="#">#Medical</a>
                            <h3>
                                <a href="donation-details.html">Need help for treatment</a>
                            </h3>
                            <p>We exist for non-profits, social enterprises, activists. Lorem politicians  and individual citizens.</p>
                        </div>
                        <div class="img">
                            <img src="{{ asset('/public/homepage/assets/img/donation/donation1.jpg')}}" alt="Donation">
                            <a class="common-btn" href="donation-details.html">Donate Now</a>
                        </div>
                        <div class="inner">
                            <div class="bottom">
                                <div class="skill">
                                    <div class="skill-bar skill1 wow fadeInLeftBig">
                                        <span class="skill-count1">85%</span>
                                    </div>
                                </div>
                                <ul>
                                    <li>Raised: $5,500.00</li>
                                    <li>Goal: $7,000.00</li>
                                </ul>
                                <h4>Donated by <span>60 people</span></h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="donation-item">
                        <div class="top">
                            <a class="tags" href="#">#Education</a>
                            <h3>
                                <a href="donation-details.html">Education for poor children</a>
                            </h3>
                            <p>We exist for non-profits, social enterprises, activists. Lorem politicians  and individual citizens.</p>
                        </div>
                        <div class="img">
                            <img src="{{ asset('/public/homepage/assets/img/donation/donation2.jpg')}}" alt="Donation">
                            <a class="common-btn" href="donation-details.html">Donate Now</a>
                        </div>
                        <div class="inner">
                            <div class="bottom">
                                <div class="skill">
                                    <div class="skill-bar skill2 wow fadeInLeftBig">
                                        <span class="skill-count2">95%</span>
                                    </div>
                                </div>
                                <ul>
                                    <li>Raised: $6,500.00</li>
                                    <li>Goal: $8,050.00</li>
                                </ul>
                                <h4>Donated by <span>50 people</span></h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="donation-item">
                        <div class="top">
                            <a class="tags" href="#">#Funding</a>
                            <h3>
                                <a href="donation-details.html">Funding for family</a>
                            </h3>
                            <p>We exist for non-profits, social enterprises, activists. Lorem politicians  and individual citizens.</p>
                        </div>
                        <div class="img">
                            <img src="{{ asset('/public/homepage/assets/img/donation/donation4.jpg')}}" alt="Donation">
                            <a class="common-btn" href="donation-details.html">Donate Now</a>
                        </div>
                        <div class="inner">
                            <div class="bottom">
                                <div class="skill">
                                    <div class="skill-bar skill4 wow fadeInLeftBig">
                                        <span class="skill-count4">80%</span>
                                    </div>
                                </div>
                                <ul>
                                    <li>Raised: $5,56.00</li>
                                    <li>Goal: $6,85.00</li>
                                </ul>
                                <h4>Donated by <span>30 people</span></h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="donation-item">
                        <div class="top">
                            <a class="tags" href="#">#Funding</a>
                            <h3>
                                <a href="donation-details.html">Funding for family</a>
                            </h3>
                            <p>We exist for non-profits, social enterprises, activists. Lorem politicians  and individual citizens.</p>
                        </div>
                        <div class="img">
                            <img src="{{ asset('/public/homepage/assets/img/donation/donation4.jpg')}}" alt="Donation">
                            <a class="common-btn" href="donation-details.html">Donate Now</a>
                        </div>
                        <div class="inner">
                            <div class="bottom">
                                <div class="skill">
                                    <div class="skill-bar skill4 wow fadeInLeftBig">
                                        <span class="skill-count4">80%</span>
                                    </div>
                                </div>
                                <ul>
                                    <li>Raised: $5,56.00</li>
                                    <li>Goal: $6,85.00</li>
                                </ul>
                                <h4>Donated by <span>30 people</span></h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="donation-item">
                        <div class="top">
                            <a class="tags" href="#">#Relief</a>
                            <h3>
                                <a href="donation-details.html">Relief for cyclone-affected</a>
                            </h3>
                            <p>We exist for non-profits, social enterprises, activists. Lorem politicians  and individual citizens.</p>
                        </div>
                        <div class="img">
                            <img src="{{ asset('/public/homepage/assets/img/donation/donation1.jpg')}}" alt="Donation">
                            <a class="common-btn" href="donation-details.html">Donate Now</a>
                        </div>
                        <div class="inner">
                            <div class="bottom">
                                <div class="skill">
                                    <div class="skill-bar skill5 wow fadeInLeftBig">
                                        <span class="skill-count5">75%</span>
                                    </div>
                                </div>
                                <ul>
                                    <li>Raised: $5,5.00</li>
                                    <li>Goal: $3,85.00</li>
                                </ul>
                                <h4>Donated by <span>20 people</span></h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="donation-item">
                        <div class="top">
                            <a class="tags" href="#">#Drought</a>
                            <h3>
                                <a href="donation-details.html">Relief for drought-affected</a>
                            </h3>
                            <p>We exist for non-profits, social enterprises, activists. Lorem politicians  and individual citizens.</p>
                        </div>
                        <div class="img">
                            <img src="{{ asset('/public/homepage/assets/img/donation/donation2.jpg')}}" alt="Donation">
                            <a class="common-btn" href="donation-details.html">Donate Now</a>
                        </div>
                        <div class="inner">
                            <div class="bottom">
                                <div class="skill">
                                    <div class="skill-bar skill6 wow fadeInLeftBig">
                                        <span class="skill-count6">70%</span>
                                    </div>
                                </div>
                                <ul>
                                    <li>Raised: $9,5.00</li>
                                    <li>Goal: $3,84.00</li>
                                </ul>
                                <h4>Donated by <span>10 people</span></h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Donation -->

    <!-- Events -->
    <section class="event-area pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">Our events</span>
                <h2>Upcoming events near you</h2>
            </div>
            <div class="row align-items-center">

                <div class="col-lg-6">
                    <div class="event-item">
                        <img src="{{ asset('/public/homepage/assets/img/event/event3.jpg') }}" alt="Event">
                        <div class="inner">
                            <h4>10 <span>Jan</span></h4>
                            <h3>
                                <a href="event-details.html">Relief giving - Providing relief</a>
                            </h3>
                            <ul>
                                <li>
                                    <i class="icofont-stopwatch"></i>
                                    <span>3.00pm - 4.00pm</span>
                                </li>
                                <li>
                                    <i class="icofont-location-pin"></i>
                                    <span>USA</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">

                    <div class="event-item-right">
                        <h4>06 <span>Jan</span></h4>
                        <h3>
                            <a href="event-details.html">Challenge is right for you</a>
                        </h3>
                        <ul>
                            <li>
                                <i class="icofont-stopwatch"></i>
                                <span>10.00am - 11.00am</span>
                            </li>
                            <li>
                                <i class="icofont-location-pin"></i>
                                <span>UK</span>
                            </li>
                        </ul>
                    </div>

                    <div class="event-item-right">
                        <h4>07 <span>Jan</span></h4>
                        <h3>
                            <a href="event-details.html">Fundraising is going</a>
                        </h3>
                        <ul>
                            <li>
                                <i class="icofont-stopwatch"></i>
                                <span>11.00am - 12.00pm</span>
                            </li>
                            <li>
                                <i class="icofont-location-pin"></i>
                                <span>France</span>
                            </li>
                        </ul>
                    </div>

                    <div class="event-item-right">
                        <h4>08 <span>Jan</span></h4>
                        <h3>
                            <a href="event-details.html">Bowling for a cause</a>
                        </h3>
                        <ul>
                            <li>
                                <i class="icofont-stopwatch"></i>
                                <span>1.00pm - 1.30pm</span>
                            </li>
                            <li>
                                <i class="icofont-location-pin"></i>
                                <span>Spain</span>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- End Events -->

    <!-- Counter -->
    <div class="counter-area pt-100 pb-70">
        <div class="container">
            <div class="row">

                <div class="col-6 col-sm-6 col-lg-3">
                    <div class="counter-item">
                        <i class="flaticon-index"></i>
                        <h3>
                            <span class="odometer" data-count="30">00</span>
                            <span class="target">M</span>
                        </h3>
                        <p>Total fund raised</p>
                    </div>
                </div>

                <div class="col-6 col-sm-6 col-lg-3">
                    <div class="counter-item">
                        <i class="flaticon-event"></i>
                        <h3>
                            <span class="odometer" data-count="250">00</span>
                            <span class="target">+</span>
                        </h3>
                        <p>Successful events</p>
                    </div>
                </div>

                <div class="col-6 col-sm-6 col-lg-3">
                    <div class="counter-item">
                        <i class="flaticon-charity"></i>
                        <h3>
                            <span class="odometer" data-count="550">00</span>
                            <span class="target">+</span>
                        </h3>
                        <p>Worldwide volunteers</p>
                    </div>
                </div>

                <div class="col-6 col-sm-6 col-lg-3">
                    <div class="counter-item">
                        <i class="flaticon-helping-hand"></i>
                        <h3>
                            <span class="odometer" data-count="500">00</span>
                            <span class="target">+</span>
                        </h3>
                        <p>Our donner</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End Counter -->

    <!-- Work -->
    <section class="work-area pt-100 pb-70">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-lg-6">
                    <div class="work-content">
                        <div class="section-title">
                            <span class="sub-title">How we work</span>
                            <h2>We exist for non-profits, social enterprises, community groups</h2>
                        </div>
                        <ul>
                            <li>
                                <h3><span>01</span>Raise money from different sources</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, pariatur esse animi temporibus iusto at dolorum</p>
                            </li>
                            <li>
                                <h3><span>02</span>Giving relief in rural area all over the world</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, pariatur esse animi temporibus iusto at dolorum</p>
                            </li>
                            <li>
                                <h3><span>03</span>Gather all the money and giving relief in need</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, pariatur esse animi temporibus iusto at dolorum</p>
                            </li>
                            <li>
                                <h3><span>04</span>Go to the country that really needs help</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, pariatur esse animi temporibus iusto at dolorum</p>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="work-img">
                        <img src="{{ asset('/public/homepage/assets/img/work/work1.jpg') }}" alt="Work">
                        <img src="{{ asset('/public/homepage/assets/img/work/work2.jpg') }}" alt="Work">
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Work -->

    <!-- Gallery -->
    <section class="gallery-area two pt-100 pb-70">
        <div class="container-fluid">
            <div class="section-title">
                <span class="sub-title">Our gallery</span>
                <h2>Discover the best things we do</h2>
                <p>We exist for non-profits, social enterprises, community groups, activists,lorem politicians and individual citizens that are making.</p>
            </div>
            <div class="gallery-slider owl-theme owl-carousel">

                <div class="gallery-item">
                    <a href="{{ asset('/public/homepage/assets/img/gallery/gallery1.jpg') }}" data-lightbox="roadtrip">
                        <img src="{{ asset('/public/homepage/assets/img/gallery/gallery1.jpg') }}" alt="Gallery">
                        <i class="icofont-eye"></i>
                    </a>
                </div>

                <div class="gallery-item">
                    <a href="{{ asset('/public/homepage/assets/img/gallery/gallery2.jpg') }}" data-lightbox="roadtrip">
                        <img src="{{ asset('/public/homepage/assets/img/gallery/gallery2.jpg') }}" alt="Gallery">
                        <i class="icofont-eye"></i>
                    </a>
                </div>

                <div class="gallery-item">
                    <a href="{{ asset('/public/homepage/assets/img/gallery/gallery3.jpg') }}" data-lightbox="roadtrip">
                        <img src="{{ asset('/public/homepage/assets/img/gallery/gallery3.jpg') }}" alt="Gallery">
                        <i class="icofont-eye"></i>
                    </a>
                </div>

                <div class="gallery-item">
                    <a href="{{ asset('/public/homepage/assets/img/gallery/gallery4.jpg') }}" data-lightbox="roadtrip">
                        <img src="{{ asset('/public/homepage/assets/img/gallery/gallery4.jpg') }}" alt="Gallery">
                        <i class="icofont-eye"></i>
                    </a>
                </div>

                <div class="gallery-item">
                    <a href="{{ asset('/public/homepage/assets/img/gallery/gallery5.jpg') }}" data-lightbox="roadtrip">
                        <img src="{{ asset('/public/homepage/assets/img/gallery/gallery5.jpg') }}" alt="Gallery">
                        <i class="icofont-eye"></i>
                    </a>
                </div>

                <div class="gallery-item">
                    <a href="{{ asset('/public/homepage/assets/img/gallery/gallery6.jpg') }}" data-lightbox="roadtrip">
                        <img src="{{ asset('/public/homepage/assets/img/gallery/gallery6.jpg') }}" alt="Gallery">
                        <i class="icofont-eye"></i>
                    </a>
                </div>

            </div>
        </div>
    </section>
    <!-- End Gallery -->

    <!-- Team -->
    <section class="team-area pb-70">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">Volunteer</span>
                <h2>Meet our excellent volunteers</h2>
                <p>We exist for non-profits, social enterprises, community groups, activists,lorem politicians and individual citizens that are making.</p>
            </div>
            <div class="row">

                <div class="col-sm-6 col-lg-4">
                    <div class="team-item">
                        <div class="top">
                            <img src="{{ asset('/public/homepage/assets/img/team/team1.jpg') }}" alt="Team">
                            <ul>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="icofont-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="icofont-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="icofont-youtube-play"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="icofont-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="bottom">
                            <h3>Jenas handar</h3>
                            <span>CEO & Founder</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="team-item">
                        <div class="top">
                            <img src="{{ asset('/public/homepage/assets/img/team/team2.jpg') }}" alt="Team">
                            <ul>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="icofont-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="icofont-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="icofont-youtube-play"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="icofont-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="bottom">
                            <h3>Smithy alisha</h3>
                            <span>Manager</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 offset-sm-3 offset-lg-0 col-lg-4">
                    <div class="team-item">
                        <div class="top">
                            <img src="{{ asset('/public/homepage/assets/img/team/team3.jpg') }}" alt="Team">
                            <ul>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="icofont-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="icofont-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="icofont-youtube-play"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="icofont-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="bottom">
                            <h3>Johan mendal</h3>
                            <span>Volunteer</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Team -->

    <!-- Testimonial -->
    <section class="testimonial-area ptb-100">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">Testimonials</span>
                <h2>Review from our clients</h2>
            </div>
            <div class="testimonial-slider owl-theme owl-carousel">

                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="testimonial-img">
                            <img src="{{ asset('/public/homepage/assets/img/testimonial/testimonial1.jpg') }}" alt="Testimonial">
                            <h3>Jac Jacson</h3>
                            <span>CEO & Abanda</span>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="testimonial-content">
                            <ul>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                            </ul>
                            <p>The legal definition of a charitable organization (and of charity) varies between countries and in some instances regions of the country. The regulation, the tax treatment.</p>
                            <i class="icofont-quote-left quote"></i>
                        </div>
                    </div>
                </div>

                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="testimonial-img">
                            <img src="{{ asset('/public/homepage/assets/img/testimonial/testimonial2.jpg') }}" alt="Testimonial">
                            <h3>Tom Henry</h3>
                            <span>Manager</span>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="testimonial-content">
                            <ul>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                            </ul>
                            <p>The legal definition of a charitable organization (and of charity) varies between countries and in some instances regions of the country. The regulation, the tax treatment.</p>
                            <i class="icofont-quote-left quote"></i>
                        </div>
                    </div>
                </div>

                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="testimonial-img">
                            <img src="{{ asset('/public/homepage/assets/img/testimonial/testimonial3.jpg') }}" alt="Testimonial">
                            <h3>Micheal Shon</h3>
                            <span>Director</span>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="testimonial-content">
                            <ul>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                                <li>
                                    <i class="icofont-star checked"></i>
                                </li>
                            </ul>
                            <p>The legal definition of a charitable organization (and of charity) varies between countries and in some instances regions of the country. The regulation, the tax treatment.</p>
                            <i class="icofont-quote-left quote"></i>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Testimonial -->

    <!-- Blog -->
    <section class="blog-area three pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">Latest news & blog</span>
                <h2>Latest charity blog</h2>
                <p>We exist for non-profits, social enterprises, community groups, activists,lorem politicians and individual citizens that are making.</p>
            </div>
            <div class="row">

                <div class="col-sm-6 col-lg-4">
                    <div class="blog-item">
                        <div class="top">
                            <a href="blog-details.html">
                                <img src="{{ asset('/public/homepage/assets/img/blog/blog1.jpg') }}" alt="Blog">
                            </a>
                        </div>
                        <div class="bottom">
                            <ul>
                                <li>
                                    <i class="icofont-calendar"></i>
                                    <span>21 Jan, 2020</span>
                                </li>
                                <li>
                                    <i class="icofont-user-alt-3"></i>
                                    <span>By:</span>
                                    <a href="#">Admin</a>
                                </li>
                            </ul>
                            <h3>
                                <a href="blog-details.html">Donate for nutration less poor people</a>
                            </h3>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet cupiditate sit ducimus dolor laudantium distinction</p>
                            <a class="blog-btn" href="blog-details.html">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="blog-item">
                        <div class="top">
                            <a href="blog-details.html">
                                <img src="{{ asset('/public/homepage/assets/img/blog/blog2.jpg') }}" alt="Blog">
                            </a>
                        </div>
                        <div class="bottom">
                            <ul>
                                <li>
                                    <i class="icofont-calendar"></i>
                                    <span>22 Jan, 2020</span>
                                </li>
                                <li>
                                    <i class="icofont-user-alt-3"></i>
                                    <span>By:</span>
                                    <a href="#">Admin</a>
                                </li>
                            </ul>
                            <h3>
                                <a href="blog-details.html">Charity meetup in Berline next year</a>
                            </h3>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet cupiditate sit ducimus dolor laudantium distinction</p>
                            <a class="blog-btn" href="blog-details.html">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 offset-sm-3 offset-lg-0 col-lg-4">
                    <div class="blog-item">
                        <div class="top">
                            <a href="blog-details.html">
                                <img src="{{ asset('/public/homepage/assets/img/blog/blog3.jpg') }}" alt="Blog">
                            </a>
                        </div>
                        <div class="bottom">
                            <ul>
                                <li>
                                    <i class="icofont-calendar"></i>
                                    <span>23 Jan, 2020</span>
                                </li>
                                <li>
                                    <i class="icofont-user-alt-3"></i>
                                    <span>By:</span>
                                    <a href="#">Admin</a>
                                </li>
                            </ul>
                            <h3>
                                <a href="blog-details.html">Donate for the poor people to help them</a>
                            </h3>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet cupiditate sit ducimus dolor laudantium distinction</p>
                            <a class="blog-btn" href="blog-details.html">Read More</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Blog -->

    @include('partials.footer')

    <!-- Go Top -->
    <div class="go-top">
        <i class="icofont-arrow-up"></i>
        <i class="icofont-arrow-up"></i>
    </div>
    <!-- End Go Top -->
@endsection
