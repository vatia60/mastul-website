<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\MainController::class, 'index'])->name('home');
Route::get('/about', [App\Http\Controllers\MainController::class, 'about'])->name('about');
Route::get('/faq', [App\Http\Controllers\MainController::class, 'faq'])->name('faq');
Route::get('/newsstory', [App\Http\Controllers\MainController::class, 'newsstory'])->name('newsstory');
Route::get('/project', [App\Http\Controllers\MainController::class, 'project'])->name('project');
Route::get('/members', [App\Http\Controllers\MainController::class, 'members'])->name('members');
Route::get('/termscondition', [App\Http\Controllers\MainController::class, 'termscondition'])->name('termscondition');
Route::get('/donate', [App\Http\Controllers\MainController::class, 'donate'])->name('donate');
Route::get('/donatedetails', [App\Http\Controllers\MainController::class, 'donatedetails'])->name('donatedetails');
Route::get('/sponsor', [App\Http\Controllers\MainController::class, 'sponsor'])->name('sponsor');
Route::get('/fundraiser', [App\Http\Controllers\MainController::class, 'fundraiser'])->name('fundraiser');
Route::get('/volunteer', [App\Http\Controllers\MainController::class, 'volunteer'])->name('volunteer');
Route::get('/contact', [App\Http\Controllers\MainController::class, 'contact'])->name('contact');

Route::prefix('admin')->group(function () {
    Route::get('/login', [App\Http\Controllers\LoginRegisterController::class, 'login'])->name('login');
    Route::get('/register', [App\Http\Controllers\LoginRegisterController::class, 'register'])->name('register');
});
